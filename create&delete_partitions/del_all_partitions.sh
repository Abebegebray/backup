#!/usr/bin/env bash

########################################################################
#Created by: A.G.
#Purpose: Exercise
#Date: 23/10/2020
#version: 1.0.0
########################################################################


disk=$1
mnt_map=$2


main() {

if_entered_path

u_mount $d_mount

_sleep

rm_partition $disk
printf "%s\n The End  \n "
_sleep
clear

}
# Check if I entered path name


if_entered_path() {


local chack1=$disk
local chack2=$mnt_map


if [[ -z $chack1 ]] ; then
         printf "%s\n Wrong \n Insert the disk path \n"
      _sleep
         clear
         exit 1
elif [[ -z $chack2 ]] ; then
         printf "%s\n Wrong \n Insert the disk path \n"
       _sleep
         clear
         exit 1

else 

	printf "%s\n entered path name \n"
	_sleep
	clear

 fi
}

#Check for mounted disks



u_mount() {


local _mount=$mnt_map
local count="1"

num_mounts=($(cat /proc/mounts | grep /mnt/ | wc -l))

if [[ $num_mounts == 0 ]]; then
        printf "%s\n not mounted \n "
else

while [ $count -le $num_mounts ]; do

umount $_mount$count 2> /dev/null
printf "%s \n umounted \n"
((count++))
done

_sleep
clear
fi
}







#Look for partitions and delete them.

rm_partition() {

local _disk=$disk
local count="1"


num_part=($(fdisk -l | grep ^$_disk | wc -l))

if [[ $num_part -eq 0 ]]; then

printf "%s\n @@@ no partitions @@@\n\n "

else

while [ $count -le $num_part ]; do

parted $_disk rm $count 2> /dev/null
printf "%s \n removed partition \n"
((count++))

done
_sleep
clear
fi
}

_sleep() {
sleep 2
}
main $@

