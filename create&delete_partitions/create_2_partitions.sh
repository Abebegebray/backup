#!/usr/bin/env bash


########################################################################
#Created by: A.G.
#Purpose: Exercise
#Date: 24/10/2020
#version: 1.0.0
########################################################################


disk=$1
count="1"
disk_ex=($(fdisk -l | grep $disk | awk '{print substr($2, 1, length($2)-1)}'))

part_ex=($(fdisk -l | grep ^$disk | wc -l))

main() {


if_entered_path

chack_if_exist_part

create_2_part

}

# Check if I entered paths name


if_entered_path() {


local chack=$disk


if [[ -z $chack ]] ; then
         printf "%s\n Wrong \n Insert the disk path \n"
       sleep 2
         clear
         exit 1
else 

	printf "%s\n entered paths name \n"
	sleep 2
	clear
 fi
}



#create 2 partitons and if exist partitions delete.

create_2_part() {

if  [[ "$disk" == "$disk_ex" ]]; then
	printf 'n\np\n1\n \n+100M\nn\np\n2\n \n+100M\nw' | fdisk $disk 2> /dev/null
	clear
while [ $count -le 2 ]; do
	mkfs.ext4 $disk$count 2> /dev/null
	((count++))
	clear
done
        printf "%s\n created 2 partitions  \n"
        sleep 2
	clear


fi
}

chack_if_exist_part() {

disk_part=($(fdisk -l | grep ^$disk | awk '{print substr($1, 1, length($1)-1)}'))

if [[ "$disk_part" == "$disk" ]]; then
	while [ $count -le $part_ex ]; do
        parted $disk rm $count 2> /dev/null
	printf "%s \n removed exist partitions \n"
	sleep 2
        ((count++))
done
	clear
else


disk_ex=$disk

create_2_part

fi
}


main $@

