#!/usr/bin/env bash
########################################################################
#Created by: A.G.
#Purpose: Exercise
#Date: 20/10/2020
#version: 1.0.0
########################################################################
map="$@"

l="##############################"
_time=2.5

## Check if I got a path

if [[ -z $@ ]] ; then
        printf "%s\n Wrong \n Insert the disk path \n"
        sleep 4
        clear
        exit 1

##all Functions do--> If the backup file name is exist exit, else you will back up .

back_up_mbr() {
if [[ -a backup.mbr ]] ; then
        message_exist   
        exit 1
else
      
        printf "%s\n back up mbr:\n"
        sleep $_time
        dd if=$map of=backup.mbr bs=2047 count=1
        echo ""
        message_not_exist
fi
}

home_partition() {
if [[ -a backup_home.img ]] ; then
        message_exist
        exit 1
else
        printf "%s\n Back up home partition :\n "
                sleep $_time
    dd if=$map of=backup_home.img
        message_not_exist
fi
}

backup_zip_home() {
        if [[ -a backup_zip_home.img.gz ]] ; then
        message_exist
        exit 1
else

        printf "%s\n Back up and zip home partition: \n"
        sleep $_time
        dd if=$map | gzip -c > backup_zip_home.img.gz
    message_not_exist
fi
}

backup_zip_root() {
    if [[ -a backup_zip_root.img.gz ]] ; then
        message_exist
        exit 1
else

        printf "%s\n Back up and zip root partition:\n "
        sleep $_time       
        dd if=$map | gzip -c > backup_zip_root.img.gz
        message_not_exist
fi
}

backup_zip_disk() {
    if [[ -a backup_zip_disk.img.gz ]] ; then
        message_exist
        exit 1
else

        printf "%s\n Back up and zip whole disk:\n "
        sleep $_time      
        dd if=$map | gzip -c > backup_zip_disk.img.gz
        message_not_exist
fi
}

clean_awap() {
printf ""
swapoff -a && swapon -a
printf "%s \n Check swap \n "
sleep $_time
free -h | grep Swap
sleep $_time
clear
exit 1
}

message_exist(){     
        printf "\n$l\n # %s\n$l\n" "This file is exist"
        sleep $_time
        clear
}

message_not_exist(){
        printf "\n$l\n # %s\n$l\n" "The file was created and completed"
        sleep $_time
        clear
}

menu(){
clear
echo -ne "
Back up Menu

1) Back up mbr
2) Back up home partition
3) Back up and zip home partition
4) Back up and zip root partition
5) Back up and zip whole disk
6) Clean awap
0) Exit
 Choose an option: "

        read a
        case $a in
                1) back_up_mbr ; menu ;;
                2) home_partition ; menu ;;
                3) backup_zip_home ; menu ;;
                4) backup_zip_root ; menu ;;
                5) backup_zip_disk ; menu ;;
                6) clean_awap ; menu ;;
                0) exit 0 ;;
                *) printf "Wrong option." ; WrongCommand; menu ; sleep $_time ;;
       esac
}

# Call the menu function

menu "$@"

